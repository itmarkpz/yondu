@extends('layouts.app')

@section('title', 'User Accounts - YONDU - PHP EXAM')

@section('content')

<div class="action-tool">
    <div class="row">
        <div class="col-md-12">
            <button class="button" data-toggle="modal" data-target="#modalUserForm">New User</button>
            <button :disabled="!isSelected" @click="deleteUser" class="button">Delete</button>
        </div>
    </div>
</div>

<table class="table table-bordered table-hover table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th width="20"></th>
            <th>Username</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Post Code</th>
            <th>Contact No.</th>
            <th>Address</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="user in users">
            <td><center><input type="checkbox" class="table-checkbox" name="select" @click="select(user.id)"></center></td>
            <td>@{{ user.username }}</td>
            <td>@{{ user.firstname }}</td>
            <td>@{{ user.lastname }}</td>
            <td>@{{ user.email }}</td>
            <td>@{{ user.postcode }}</td>
            <td>@{{ user.contact_number }}</td>
            <td>@{{ user.address }}</td>
            <td><center><button class="btn btn-primary button" @click="editUser(user.id)">Edit</button></center></td>
        </tr>
    </tbody>
</table>

<form action="#" @submit.prevent="submitForm" method="POST">
    <div class="modal fade" id="modalUserForm" tabindex="-1" role="dialog" aria-labelledby="modalUserLabel" aria-hidden="true"> 
        <div class="modal-dialog">
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="modalUserLabel">User Form</h4>
             </div>
             <div class="modal-body">
                <div class="alert alert-warning" v-if="!isValid">
                    <b>Please complete the following field(s):</b>
                    <ul>
                      <li v-show="!formValidation.username">Username.</li>
                      <li v-show="!formValidation.password">Password.</li>
                      <li v-show="!formValidation.firstname">First Name.</li>
                      <li v-show="!formValidation.lastname">Last Name.</li>
                      <li v-show="!formValidation.contact_number">Contact Number.</li>
                      <li v-show="!formValidation.postcode">Post Code.</li>
                      <li v-show="!formValidation.email">Email Address.</li>
                      <li v-show="!formValidation.address">Home Address.</li>
                    </ul>
                </div>
                <div class="form-group">
                    <label class="control-label" for="username">Username&nbsp;*</label>
                    <input type="text" name="username" class="form-control" id="username" v-model="username">
               </div>
               <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="password">Password&nbsp;*</label>
                            <input type="password" name="password" class="form-control" id="password" v-model="password">
                       </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="repassword">Retype Password&nbsp;*</label>
                            <input type="password" name="repassword" class="form-control" id="repassword" v-model="repassword">
                       </div>
                    </div>
               </div>
               <hr>
               <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="firstname">First Name&nbsp;*</label>
                            <input type="text" name="firstname" class="form-control" id="firstname" v-model="firstname">
                       </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="lastname">Last Name&nbsp;*</label>
                            <input type="text" name="lastname" class="form-control" id="lastname" v-model="lastname">
                       </div>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="email">Email Address&nbsp;*</label>
                            <input type="text" name="email" class="form-control" id="email" v-model="email">
                       </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="contact_number">Contact Number&nbsp;*</label>
                            <input type="text" name="contact_number" class="form-control" id="contact_number" v-model="contact_number">
                       </div>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="postcode">Post Code&nbsp;*</label>
                            <input type="text" name="postcode" class="form-control" id="postcode" v-model="postcode">
                       </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="control-label" for="address">Address&nbsp;*</label>
                            <textarea class="form-control" name="address" id="address" v-model="address" style="resize: none;"></textarea>
                       </div>
                    </div>
               </div>
             </div>
            <div class="modal-footer">
                <button :disabled="!isValid" type="submit" class="button">Submit</button>
             </div>
        </div>
    </div>
</form>

@endsection
