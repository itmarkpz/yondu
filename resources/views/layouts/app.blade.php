<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="token" id="token" value="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Mukta+Mahee" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bundle.css') }}">
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="{{ url('/') }}">
                YONDU - PHP EXAM
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @auth
                <li><a href="javascript:void(0)">{{ Auth::user()->role }}&nbsp;-&nbsp;{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</a></li>
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
                @endauth
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        @yield('breadcrumb')
        <div id="app">
            @yield('content')
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('/js/bundle.js') }}"></script>
@stack('scripts')
</body>
</html>