var app = new Vue({
    http: {
        root: '/root',
        headers: {
          'X-CSRF-TOKEN': document.querySelector('#token').getAttribute('value')
        }
    },

    el: '#app',

    data: {
        users: [],
        errors: [],
        selected: [],
        edit: false,
        id: '',
        username: '',
        password: '',
        repassword: '',
        firstname: '',
        lastname: '',
        email: '',
        contact_number: '',
        postcode: '',
        address: ''
    },

    methods: {

        getUsers: function() {
            this.$http.get('/api/users').then(function(response) {
                this.$set(this, 'users', response.data);
            })
        },

        editUser: function(id) {
            this.edit = true;

            this.$http.get('/api/users/edit/' + id).then(function(response) {
                var user = response.data;

                this.id = user.id;
                this.username = user.username;
                this.email = user.email;
                this.firstname = user.firstname;
                this.lastname = user.lastname;
                this.postcode = user.postcode;
                this.contact_number = user.contact_number;
                this.address = user.address;

                $('#modalUserForm').modal('show');
            });
        },

        deleteUser: function() {
            var message = confirm('Proceed to deleted the selected user?');

            if (message == true) {
                this.$http.post('/api/users/delete', {id: this.selected}).then(function() {
                    this.getUsers();
                    this.selected = [];
                    $('input.table-checkbox').prop('checked', false);
                    alert('Selected user(s) was successfully deleted.');
                });
            }
            
        },

        submitForm: function() {
            var data = {
                username: this.username,
                password: this.password,
                email: this.email,
                firstname: this.firstname,
                lastname: this.lastname,
                postcode: this.postcode,
                contact_number: this.contact_number,
                address: this.address
            };

            if (this.edit) {
                data.id = this.id;
                this.$http.post('/api/users/update', data).then(function() {
                    this.clearForm();
                    this.getUsers();
                    $('#modalUserForm').modal('hide');
                    alert('User was successfully updated.');
                });

            } else {
                this.$http.post('/api/users/create', data).then(function() {
                    this.clearForm();
                    this.getUsers();
                    $('#modalUserForm').modal('hide');
                    alert('New user was successfully created.');
                });

            }
            
        },

        isEmail: function(email) {
            var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return regex.test(email);
        },

        isNumber: function(num) {
            return (!isNaN(num) && !!num);
        },

        isEqual: function(one, two) {
            return (!!one.trim() && !!two.trim() && one.trim() == two.trim());
        },

        clearForm: function() {
            this.id = '';
            this.username = '';
            this.password = '';
            this.repassword = '';
            this.email = '';
            this.firstname = '';
            this.lastname = '';
            this.postcode = '';
            this.contact_number = '';
            this.address = '';
        },

        select: function(id) {
            if (this.selected.includes(id)) {
                this.selected.pop(id);
            } else {
                this.selected.push(id);
            }
            console.log(this.selected);
        }

    },

    computed: {
        formValidation: function() {
            return {
                email: this.isEmail(this.email),
                contact_number: this.isNumber(this.contact_number),
                postcode: this.isNumber(this.postcode),
                username: !!this.username.trim(),
                password: this.isEqual(this.password, this.repassword),
                firstname: !!this.firstname.trim(),
                lastname: !!this.lastname.trim(),
                address: !!this.address.trim()
            }
        },

        isValid: function() {
            var validation = this.formValidation;

            return Object.keys(validation).every(function(key) {
                return validation[key];
            });
        },

        isSelected: function() {
            return (this.selected.length > 0);
        }
    },

    mounted: function() {
        this.getUsers();
    }

});