<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all users with rules is equal to "user".
     *
     * @return array
     */
    public function showNormalUserOnly()
    {
        return User::where(['role' => 'user'])
                   ->get();
    }

    /**
     * Create new user.
     *
     * @return array
     */
    public function createUser(Request $request)
    {
        $user = $request->all();
        $user['password'] = Hash::make($request->password);

        return User::create($user);
    }

    /**
     * Get the details of individual user.
     *
     * @return array
     */
    public function getUserInfo($id)
    {
        return User::findOrFail($id);
    }

    /**
     * Update user information.
     *
     * @return array
     */
    public function updateUser(Request $request)
    {
        $id = $request->id;

        $user = $request->all();
        $user['password'] = Hash::make($request->password);
        unset($user['id']);
        
        User::findOrFail($id)
            ->update($user);

        return Response::json($request->all());
    }

    /**
     * Delete selected user(s).
     *
     * @return array
     */
    public function deleteUser(Request $request)
    {
        $id = $request->id;

        User::whereIn('id', $id)
            ->delete();

        return Response::json($request->all());
    }
}
