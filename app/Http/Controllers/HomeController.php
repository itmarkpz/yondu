<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard or user account list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $template = 'home';
        
        if (Auth::user()->role == 'admin') $template = 'users';
        
        return view($template);
    }
}
