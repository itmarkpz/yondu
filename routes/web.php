<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Login control exlude register rounte

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Home

Route::get('/', 'HomeController@index')->name('home');

// API

Route::group(['prefix' => 'api/users'], function() {
    Route::get('/', 'UserController@showNormalUserOnly');
    Route::get('edit/{id}','UserController@getUserInfo');
    Route::post('create', 'UserController@createUser');
    Route::post('update', 'UserController@updateUser');
    Route::post('delete', 'UserController@deleteUser');
});
