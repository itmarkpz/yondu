let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

var npmFolder = 'node_modules/',
    jsLaravelFolder = 'resources/assets/js/',
    cssLaravelFolder = 'resources/assets/css';

mix.copy(npmFolder + 'jquery/dist/jquery.js', jsLaravelFolder);
mix.copy(npmFolder + 'bootstrap3/dist/js/bootstrap.js', jsLaravelFolder);
mix.copy(npmFolder + 'vue/dist/vue.js', jsLaravelFolder);
mix.copy(npmFolder + 'vue-resource/dist/vue-resource.js', jsLaravelFolder);

mix.scripts([
    'resources/assets/js/jquery.js',
    'resources/assets/js/bootstrap.js',
    'resources/assets/js/vue.js',
    'resources/assets/js/vue-resource.js',
    'resources/assets/js/action.js'
], 'public/js/bundle.js');

mix.copy(npmFolder + 'bootstrap3/dist/css/bootstrap.css', cssLaravelFolder);

mix.styles([
    'resources/assets/css/bootstrap.css',
    'resources/assets/css/custom.css',
    'resources/assets/css/theme.css'
], 'public/css/bundle.css');