<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;
use App\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->text('address');
            $table->integer('postcode');
            $table->string('contact_number');
            $table->string('email');
            $table->string('username');
            $table->string('password');
            $table->enum('role', ['admin', 'user'])->default('user');
            $table->rememberToken();
            $table->timestamps();
        });

        $default_admin = [
            'username'       => 'admin',
            'password'       => Hash::make('admin'),
            'firstname'      => 'Juan',
            'lastname'       => 'Dela Cruz',
            'role'           => 'admin',
            'address'        => '123 Street Makati City PH',
            'postcode'       => '1234',
            'contact_number' => '09123456789',
            'email'          => 'admin.juan@example.com'
        ];

        User::insert($default_admin);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
